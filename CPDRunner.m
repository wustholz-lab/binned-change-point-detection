function CPDRunner()
% Starts CPD. Input confidence into the command line. Will
% prompt file selection upon starting. Saves files outputted from CPD.

DISP_GFX = false; % Boolean- display CPD graphical output when running
confidence = '90'; % Only confidence available is 90%

start=pwd;
[file,path]=uigetfile('*.txt','Select file(s) to be analyzed','MultiSelect','on');

cd(path)
[~, ~] = mkdir(strcat('./Output',confidence));

if iscell(file)
    for i=1:size(file,2)
        [~,file_str]= fileparts(string(file(i)));
        CPD(string(file(i)),confidence);
        saveas(gcf,strcat('./Output', confidence,'/',file_str,'.jpg'))
        saveas(gcf,strcat('./Output', confidence,'/',file_str,'.fig'))
        movefile(strcat('CPD',string(file(i))),strcat('./Output',confidence,'/','CPD',string(file(i))))
        if ~DISP_GFX
            close(gcf);
        end
    end
elseif ischar(file)
    [~,file_str]= fileparts(file);
    CPD(file, confidence);
        saveas(gcf,strcat('./Output',confidence,'/',file_str,'.jpg'))
        saveas(gcf,strcat('./Output',confidence,'/',file_str,'.fig'))
        movefile(strcat('CPD',file),strcat('./Output',confidence,'/','CPD',file))
    if ~DISP_GFX
        close(gcf);
    end
else
    disp('Canceled')
end
disp(path)
cd(start)
disp("Analysis Finished!")
end




