function [cps, intens, leftcb, rightcb]=CPDetector(trace,Confidence,conf)
% Finds statistically significant change points using algorithm from Haw
% Yang et. al. https://doi.org/10.1021/jp0467548. Algorithm is as follows:
% "2.2.2... To find all the significant change points,we use a recursive
% binary segmentation algorithm. First, a change point is found by
% applying eq 5 to the entire single-molecule time trajectory. The maxiumum
% of the log-likehood ratio, eq 6, is considered as a change point and its
% confidence itnerval is found using eq 9. The left (right) confidence bound
% is then held as one end point, with the start (end) of the trajectory
% serving as the other end point. The search for change points is continued
%  in the left (right) daughter set. This procedure is repeated recursively
%  until no further change points are found. - pg 621, Haw Yang

% Initialization of variables
max_ratio=-Inf;
max_ratio_index=NaN;
max_ratio_intens=NaN;
S=sum(trace,1); % Total sum of trace intensities
Sk=0; % Running sum of trace intensities
n=size(trace,1); % Number of bins

%If the sum of the photons is not 0 and there are at least two bins of data
if S~=0 && n>1
    
    %Loop through k values, ranging from 1 to n-1, where n = number of bins
    for k=1:n-1
        
        % Running sum of photon intensities. Add new bin intensity to sum
        Sk=Sk+trace(k);
        
        % There are two special cases and one general case. The two special
        % cases utilize the same equation as the general case, but some
        % values are omitted as the equal 0. In all cases, -S*log(S/n) is
        % ignored when determining the max ratio and added back in later to
        % increase speed. The equation for the ratio is found on pg 415,
        % "On Exact Inference for Change in a Poisson Sequence", 
        % https://doi.org/10.1081/STA-100002089
        
        if Sk==0 % Special case 1. Sk*log(Sk/k)= 0 at beginning of trace
            ratio=S*log(S/(n-k));
        elseif Sk==S %Special case 2. Snk*log(Snk/(n-k))= 0 at end of trace
            ratio=S*log(S/k);
        else % General case
            Snk=S-Sk; %Compute sum of the intensities after point k.
            ratio=Sk*log(Sk/k)+Snk*log(Snk/(n-k));
        end
        
        % If a bigger ratio is found, update max_ratio. (eq 5, Haw Yang)
        if ratio>max_ratio
            max_ratio=ratio;
            max_ratio_index=k;
            max_ratio_intens=Sk/k; % Average intensity
        end
    end
    
    % If the number of bins is greater or equal to 5000, use the critical
    % values generated for n = 5000. It is difficult to produce critical
    % values beyond n = 5000 as they take weeks to generate.
    if n>=5000
        file=fopen('CritValues5000.txt','r');
        
    % Else, find the critical values corresponding to the number of bins    
    else
        file=fopen(strcat('CritValues',int2str(n),'.txt'),'r');
    end
    
    % If the total sum of photons is greater or equal to 1000, use the
    % critical values generated for S = 1000. It is difficult to produce
    % critical values beyond S = 1000 as they take weeks to generate.
    if S>=1000
        fseek(file,(1000-1)*7,'bof');
        CritValue=str2double(fgetl(file));
   % Else, find the critical value corresponding to the sum of photons      
    else
        fseek(file,(S-1)*7,'bof');
        CritValue=str2double(fgetl(file));
    end
    fclose(file);
    
    % If a change point was found, test its confidence using eq 9 from
    % Yang. This is the recusive case
    if max_ratio-S*log(S/n)>CritValue %Likelihood Ratio Corrected Here
        if Confidence
            [leftbound,rightbound]=CPConfidence(max_ratio_index,trace,max_ratio-S*log(S/n));
            
            % Recursive calls for left and right sides of change point
            [leftcps,leftintens,llb,lrb]=CPDetector(trace(1:leftbound-1),Confidence,conf);
            [rightcps,rightintens,rlb,rrb]=CPDetector(trace(rightbound+1:end),Confidence,conf);
            
            % Combine left and right change point lists and add new change
            % point. Do the same for intensity and confidence bounds.
            cps=[leftcps;max_ratio_index;rightbound+rightcps];
            intens=[leftintens;max_ratio_intens;rightintens];
            leftcb=[llb;leftbound;rlb+rightbound];
            rightcb=[lrb;rightbound;rrb+rightbound];
        else
            [leftcps,leftintens]=CPDetector(trace(1:max_ratio_index),Confidence,conf);
            [rightcps,rightintens]=CPDetector(trace(max_ratio_index+1:end),Confidence,conf);
            cps=[leftcps;max_ratio_index;max_ratio_index+rightcps];
            intens=[leftintens;max_ratio_intens;rightintens];
            leftcb=[];
            rightcb=[];
        end
        
    % Base case     
    else
        cps=[];
        intens=[];
        leftcb=[];
        rightcb=[];
    end
    
% Sum of photons is either 0 or there is 0 or 1 bins.    
else
    cps=[];
    intens=[];
    leftcb=[];
    rightcb=[];
end
end