function [layer,layeridentity]=Agglomeration2(layer,tot,layeridentity)
% Reduces the number of unique intensity states to be more in line with the
% discrete number of states expected to see. Details in section 2.3.1 of
% Yang paper, pg 622. 

% Initialize max likelihood tracker
max=-Inf;

% Iterate by 1, going from 1 to tot, the number of segments before this
% cycle of agglomeration. (The first iteration will go from 1 to n+1, where
% n is the number of change points). Refer to eq 11, 12, 13 in Yang paper.
for i=1:tot
    % Determine average intensity (ni/Ti) for the ith segment. This
    % average intensity will be compared to all other segments
    lambda1=layer(i,1)/layer(i,2);
    % Iterate by 1, going from i+1 to tot. (if i=1 and tot=16, j will
    % range iterate 2,3,4,...,16)
    for j=i+1:tot
        % Determine average intensity (nj/Tj) for the jth segment (always
        % different than the ith)
        lambda2=layer(j,1)/layer(j,2);
        
        % Determine average intensity if ith and jth term were combined
        % (ni+nj)/(Ti+Tj).
        lambda3=(layer(i,1)+layer(j,1))/(layer(i,2)+layer(j,2));
        
        % Compute log-likelihood ratio (merit function, eq 11)
        likelihood=-lambda1*layer(i,1)-lambda2*layer(j,1)+lambda3*(layer(i,1)+layer(j,1));
        
        % Compare likelihoods. Change markers if new likelihood is higher
        if likelihood>max
            max=likelihood;
            group1=i;
            group2=j;
        end
    end
end

% Merge the two segments that were the most similar. First, merge their
% intensity then merge their durations
layer(group1,1)=layer(group1,1)+layer(group2,1);
layer(group1,2)=layer(group1,2)+layer(group2,2);

% Set the now unnecessary second group to empty
layer(group2,:)=[];

% Swap the layer identities to reflect the updated layer change 
for i=1:size(layeridentity,1)
    if layeridentity(i)==group2
        layeridentity(i)=group1;
        
    % Shift all other layer identities down to fill in gap.
    elseif layeridentity(i)>group2
        layeridentity(i)=layeridentity(i)-1;
    end
end
end