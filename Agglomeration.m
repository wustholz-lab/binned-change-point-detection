function [groups,identity]=Agglomeration(cps,binned_photons)
% Averages segments with similar mean intensities. groups is a 2D array
% with columns of photon count and duration. The rows are organized in sets
% of segments. The bottom n+1 rows of groups correspond to the segments
% created by CPDetector. The n rows above that set correspond to the
% original segments, except the two most similar segments were averaged
% together. The next n-1 rows follows the same pattern and the pattern
% continues until there is only 1 intensity. Details in section 2.3.1 of
% Yang paper, pg 625
if isempty(cps)
    return
end

% Initialize 2D array with 2 columns, and n+1 rows where n is the number of
% change points.
layer=zeros(size(cps,1)+1,2);

% The first column of the layer array equals the number of photons in each
% change point segment. Note: There are n+1 segments for n change points
layer(1,1)=sum(binned_photons(1:cps(1)),1);
for i=2:size(cps,1)
    layer(i,1)=sum(binned_photons(cps(i-1)+1:cps(i)),1);
end
layer(end,1)=sum(binned_photons(cps(end):end),1);

% The second column of the layer array equals the duration of each segment
layer(1,2)=cps(1);
for i=2:size(cps,1)
    layer(i,2)=cps(i)-cps(i-1);
end
layer(end,2)=size(binned_photons,1)-cps(end);

% Create 1D array of [1,2,3,...,n+1] where n is the number of change points
layeridentity=[1:size(layer,1)]';

% Initialize 2D array with 2 columns and (n+1)(n+2)/2 rows 
groups=zeros(sum(1:size(cps,1)+1),2);

% Initialize(n+1)th dimensional array where n is the number of change
% points. Note again that there are n+1 segments for n change points
identity=zeros(size(cps,1)+1,size(cps,1)+1);

% Set the bounds to be the same size as the layer array and input the layer
% array into the groups array.
left=sum(1:size(cps,1))+1;
right=size(groups,1);
groups(left:right,:)=layer;

% Add the identities of the layer to the last column of the nth dimensional
% identity array
identity(:,end)=layeridentity;

% Iterate backwards by 1, going from n to -1, where n is the number of
% change points
for i=size(cps,1):-1:1
    % Allot space for the next iteration of agglomerated intensities by
    % setting the end (right) bound one above the first entry of the
    % previous iteration and the start (left) bound i above the first entry
    % of the previous iteration. i corresponds to the number of segments
    % after a cycle of the agglomeration algorithm. Thus, i-1 is the number
    % of change points after the same cycle.
    nright=left-1;
    nleft=left-i;
    
    % Input the most recently created set of segments and output the new
    % set of segments created from that into the groups array
    [groups(nleft:nright,:),identity(:,i)]=Agglomeration2(groups(left:right,:),i+1,identity(:,i+1));
    
    % Rearrange the indices for next loop
    left=nleft;
    right=nright;
end
end