# Change Point Detection for Binned Single-Molecule Data
The change point detection (CPD) method finds discrete changes in binned single-molecule data, outputting statistically-significant intensities and corresponding temporal durations. This CPD algorithm largely follows the prescription outlined by L. Watkins and H. Yang (https://doi.org/10.1021/jp0467548) where the locations of the intensity change points are determined recursively via a generalized likelihood ratio test and then clustered together to form the true number of states. However, our method makes a subtle, yet important, differentiation in the critical values used to detect change points. Yang’s original CPD method was developed for detecting change points from a series of individual photon arrival times and not binned data. To address this issue, we utilize the derivation by Boudjellaba et al. (https://doi.org/10.1081/STA-100002089) to calculate critical values and detect change points for binned data. The program output includes statistically-significant intensities, corresponding durations, as well as a number of blinking statistics.

Written by Harrison G. Tuckman, Grayson R. Hoy, Kelly M. Kopera, Walker T. Knapp, Kristin L. Wustholz

Please cite the following reference: Kopera, K. M; Tuckman, H. R.; Hoy, G. R.; Wustholz, K. L. Origin of Kinetic Dispersion in Eosin-Sensitized TiO2: Insights from Single-Molecule Spectroscopy, <em>J. Phys. Chem. C</em> **2021**, 125, 43, 23634–23645. https://doi.org/10.1021/acs.jpcc.1c07597  [doi:10.5281/zenodo.10471847](doi:10.5281/zenodo.10471847)

## Syntax
```
CPDRunner()
```

## Input
* `Files.txt` — After calling CPDRunner, it will prompt you to select .txt files to analyze. The formatting for the .txt files is a 2D array, where the columns in order are time and intensity. The time should increase in a fixed interval length (e.g. 0.01, 0.02, 0.03)

## Outputs
All output files are stored in a folder titled `Output90`, denoting the 90% confidence used. Multiple runs of CPD will not overwrite the folder. CPD analysis produces a list of statistically-significant intensities and the corresponding durations of those events. The program further parses this data into two types of events: segments and intervals. Segments correspond to different intensity states and intervals encompass consecutive On or Off segments. Segments are categorized as On or Off depending on if they are respectively above or below a determined `Threshold`. These categorized events yield blinking statistics found in `@Summary.xlsx`.
* `CPDFileName.txt` — CPD Output. Six columns of output corresponding to (in order from left to right): intensity, duration [seconds], intensity of on segments, intensity of off segments, intensity of on intervals, intensity of off intervals. Empty cells are padded with zeros and all cells in the last row are zero.
* `FileName.fig` — MATLAB Figure of analysis plot. x-axis is time in units of seconds. y-axis is intensity. Number in the top right shows number of unique intensity states. Red dots indicates the time of change points, the black line is raw data, and the green line is CPD.
* `FileName.jpg` — JPG of analysis plot
* `@Summary.xlsx`— Excel sheet that summarizes the blinking statistics for each analyzed file. Multiple runs of CPD will write additional rows to the same output. `N_I` is the number of unique intensities. `N_onseg`, `N_offseg`, `N_onint`, `N_offint` refer to the number of on/off segments/intervals, accordingly. `Threshold` is the lowest deconvolved intensity level, designated as off. Levels with intensities greater than 1 standard deviation above the rms noise are denoted as on. `20% I_max` is 20% of the maximum intensity. `seg_freq` and `int_freq` refer to the number of segments/intervals.`<I>` is the average intensity. `<t_onseg>`, `<t_offseg>`, `<t_onint>`, `<t_offint>` refers to the average duration of on/off segments/intervals. Further description of these statistics are discussed in DeSalvo, G. A; Hoy, G. R.; Kogan, I. M.; Li, J. Z; Palmer, E. T.; Luz-Ricca, E.; Scemama de Gialluly, P.; Wustholz, K.L. Blinking-Based Multiplexing: A New Approach for Differentiating Spectrally Overlapped Emitters, <em>J. Phys. Chem. Lett</em>, **13**, 5056-5060. https://doi.org/10.1021/acs.jpclett.2c01252

## Optional Settings
* `BIN_TIME` — CPD.m; Adjusts the scale for Matlab plots so x-axis is in units of seconds. To adjust bin time, the user should edit CPD.m on line 5. Set by default to 0.01s, i.e., a 10ms bin time.
* `DISP_GFX` — CPDRunner.m; The program automatically displays the analyzed blinking graphs while CPD runs, but then closes these files once it has finished. To keep these graphs open after analysis, the user can edit CPDRunner.m on line 5.
