function CPD(filename,confidence)
% Main program for CPD. Takes in data from blinking trace and runs
% other algorithms

BIN_TIME = .01; % Bin time, in seconds. Used for scaling

[file_path,~,~]=fileparts(mfilename('fullpath'));
addpath(strcat(file_path,'/ConfidenceCritValues'));
addpath(strcat(file_path,'/CriticalValues90')); % 90% confidence

%Display file name
disp(filename);

%Import raw photon data. Photon data stored in two columns.
data=importdata(filename);

%Access photon intensity column
binned_photons=data(:,2);

%Access time column
times=data(:,1);

% Determines where changepoints are and organizes their time into array
[cps,~,~,~]=CPDetector(binned_photons,true,confidence); 

% groups is a list of the most likely intensities for a constrained
% number of segments, ranging from n+1 to 1, where n is the number of
% change points
if isempty(cps)
    disp('No change points detected')
    finalized_cps = cps;
    finalized_intens = [mean(binned_photons)];
    min_states = 1;
else 
    [groups,~]=Agglomeration(cps,binned_photons);

    % Mixture Model pre-processing

    % Initialize 2D array with two columns and n+1 rows, where n is the number
    % of change points. The first column is number of photons. The second
    % column is duration
    trace=zeros(size(cps,1)+1,2);

    trace(1,1)=sum(binned_photons(1:cps(1)));
    trace(1,2)=cps(1);

    for i=2:size(cps,1)
        trace(i,1)=sum(binned_photons(cps(i-1)+1:cps(i)));
        trace(i,2)=cps(i)-cps(i-1);
    end
    trace(end,1)=sum(binned_photons(cps(end)+1:end));
    trace(end,2)=size(binned_photons,1)-cps(end);

    % Convert the first column to average intensity for each interval
    trace(:,1)=trace(:,1)./trace(:,2);

    % Sort rows in increasing order of average intensity
    sorted_trace=sortrows(trace,1);

    % Initialize array
    merged_trace=[];

    % Set counter variable as the duration of lowest intensity point
    segment_duration=sorted_trace(1,2);
    % Set current variable as average intensity of lowest intensity point
    current_intensity=sorted_trace(1,1);

    % Merge together intervals with the same average intensity
    for i=2:size(sorted_trace,1)
        if sorted_trace(i,1)==current_intensity
            segment_duration=segment_duration+sorted_trace(i,2);
        else
            merged_trace=[merged_trace;current_intensity,segment_duration];
            segment_duration=sorted_trace(i,2);
            current_intensity=sorted_trace(i,1);
        end
    end
    merged_trace=[merged_trace;current_intensity,segment_duration];

    % Covert the first column back to number of photons for each segment
    merged_trace(:,1)=round(merged_trace(:,1).*merged_trace(:,2));
    clear sorted_trace

    % Initialize variables
    finished=false;
    index=1;
    max_likelihood=-Inf;

    % While not finished and index is less than the number of segments
    while ~finished && index<size(cps,1)+1
        [MM_cps,MM_intens,likelihood]=MixtureModel(groups,merged_trace,index,trace(:,1));

        % If MixtureModel returns no change points, use the original change
        % points. Probability (prob) calculated with eq. 16 in Haw Yang
        % et. al paper available at https://doi.org/10.1021/jp0467548
        if size(MM_cps,1)==0
            prob=2*likelihood-(2*index-1)*log(size(cps,1))-size(cps,1)*log(size(binned_photons,1)); %Yang eq. 16.
        else
            prob=2*likelihood-(2*index-1)*log(size(MM_cps,1))-size(MM_cps,1)*log(size(binned_photons,1)); %Yang eq. 16.
        end
        disp(prob)

        % Determine the most likely set of change points
        if prob>=max_likelihood
            max_likelihood=prob;
            finalized_cps=MM_cps;
            finalized_intens=MM_intens;
            min_states=index;
        else
            finished=true;
        end
        index=index+1;
    end
    disp(min_states)

    % Convert cps3 indices to absolute time since start of trace to prepare for
    % plotting and output
    for i=1:size(finalized_cps,1)
        finalized_cps(i)=sum(trace(1:finalized_cps(i),2));
    end
end

% Figure generation
figure('name','Mixture')

% Plot the intensities as a function of time in black
plot(times,binned_photons,'color','k');
hold on
ylim=get(gca,'ylim');
xlim=get(gca,'xlim');

% Plot CPD 
if size(finalized_cps,1)>0
    % Add bounds to x-axis
    if size(finalized_cps,1)==1
        breakptsplt=[0;repelem(finalized_cps,2)';size(binned_photons,1)];
    else
        breakptsplt=[0;repelem(finalized_cps,2);size(binned_photons,1)];
    end
    intensplt=repelem(finalized_intens,2);
    
    % Plot CPD-defined intensities as a function of time in green
    plot(breakptsplt*BIN_TIME,intensplt,'color','g','LineWidth',2)


    % At the top of the graph, plot the location of the change points in red
    plot(cps*BIN_TIME,.9*ylim(2),'.','color','r');
else
    % Special case if no blinking if found
    plot([times(1) times(end)],[finalized_intens finalized_intens],'color','g','LineWidth',2);
end

% Display in top right corner the number of unique intensity states
text(.95*xlim(2),.95*ylim(2),int2str(min_states));

if size(finalized_intens,1)>2
    % Preallocate space for array 
    output=zeros(size(finalized_intens,1)-2,6);
    
    % The first column is intensity of segment. First and last change points
    % removed as observation window arbitrarily set
    output(:,1)=finalized_intens(2:end-1);
    
    % The second column is duration of segment. First and last change points
    % removed
    output(:,2)=BIN_TIME*(finalized_cps(2:end)-finalized_cps(1:end-1));
    
    % Maximum intensity considered "off". Equal to one std above the lowest
    % deconvolved intensity
    threshold=min(finalized_intens)+sqrt(min(finalized_intens));
    
    % Initializing counters
    onSegmentIndex=1; % Counts number of on segments
    offSegmentIndex=1; % Counts number of off segments
    onIndex=1; % Counts number of on intervals
    offIndex=1; % Count number of off intervals
    segTotal=0;
    
    % Initialize variable to track whether the last segment was on/off
    % 0 is off segment. 1 is on segment. Starts at -1
    lastSeg=-1;
    
    % Loop through intensities
    for i=1:size(output,1)
        
        % If the intensity is greater than threshold...
        if output(i,1)>threshold   
            % ..set duration value of segment in onSegment column
            output(onSegmentIndex,3)=output(i,2);
            % Increment counter
            onSegmentIndex=onSegmentIndex+1;
            
            % If last segment was off...
            if lastSeg==0      
                % Record sum of all off segments in off interval
                output(offIndex,6)=segTotal;
                % Increment off interval counter
                offIndex=offIndex+1;
                % Reset sum of segments for a given interval to zero
                segTotal=0;
            end
            % Add current intensity of segment to interval intensity sum
            segTotal=segTotal+output(i,2);
            % Switch last segment to "on"
            lastSeg=1;
        % else, intensity is less or equal to threshold; thus...
        else
            % ..set duration value of segment in offSegment column
            output(offSegmentIndex,4)=output(i,2);
            offSegmentIndex=offSegmentIndex+1;

            % If last segment was on, record sum of all on segments in on
            % interval, increment interval counter, and reset sum of
            % segments to zero
            if lastSeg==1
                output(onIndex,5)=segTotal;
                onIndex=onIndex+1;
                segTotal=0;
            end
            segTotal=segTotal+output(i,2);
            lastSeg=0;
        end
    end
else
    disp('No Segments');
    output=[0,0,0,0,0,0];
    onSegmentIndex=1;
    offSegmentIndex=1;
    onIndex=1;
    offIndex=1;
    threshold=min(finalized_intens)+sqrt(min(finalized_intens));
end

% Intensity, Duration, Onsegment Offseg Onint Offint
file=fopen(strcat('CPD',filename),'w');
for i=1:size(output,1)
    fprintf(file,'%12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',output(i,:));
end
fprintf(file,'%12.8f %12.8f %12.8f %12.8f %12.8f %12.8f\n',[0 0 0 0 0 0]);
fclose(file);

% Calculate average intensity
tavintens=dot(([finalized_cps;size(binned_photons,1)]-[0;finalized_cps]),finalized_intens)/size(binned_photons,1);

% Prepare table to write Excel sheet summary.
summaryTable=table(convertCharsToStrings(filename),min_states,onSegmentIndex-1,...
    offSegmentIndex-1,onIndex-1,offIndex-1,threshold,.2*max(finalized_intens),...
    (onSegmentIndex+offSegmentIndex-2)/times(end),tavintens,mean(output(1:onSegmentIndex-1,3)),...
    mean(output(1:offSegmentIndex-1,4)),...
    mean(output(1:onIndex-1,5)),...
    mean(output(1:offIndex-1,6)),...
    (onIndex+offIndex-2)/times(end),...
    'VariableNames',["Filename","N_I","N_onseg",...
    "N_offseg","N_onint","N_offint","Threshold",...
    "20% I_max","seg_freq","<I>",...
    "<t_onseg>","<t_offseg>","<t_onint>",...
    "<t_offint>","int_freq"]);

% Write data to Excel file
cd(strcat('Output', confidence,'/'));
filname = '@Summary.xlsx';
try    
    % Error thrown if file does not exist. Catch block creates new table.
    T=readtable(filname,'Range','A:O','PreserveVariableNames', true);
    isNewData=true;
    for i=1:size(T.Filename,1)
        if strcmp(summaryTable.Filename(1),T.Filename(i))
            isNewData=false;
            disp("File already Analyzed. Skipping Recording")
        end
    end
    if isNewData
        T=[T;summaryTable];
        writetable(T,filname);
    end
catch
    writetable(summaryTable,filname);
end
cd(file_path);
end