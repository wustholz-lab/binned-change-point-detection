function [cps,intens,likelihood]=MixtureModel(groups,trace,number,origtrace)
% groups is the array with intensity (photon count) and duration for all
% possible number of segments. trace is the merged and sorted version of
% the original trace. number is the constrained number of segments.
% origtrace is the original, average intensities in sequential order
MAX_ITER = 1000;

% Grab only the rows that correspond to the number passed into the function 
groups=groups(sum(1:number-1)+1:sum(1:number),:);

% Sum the duration of the events  
observations=sum(groups(:,2),1);

% Create an array of the average intensity for each segment
lambda=groups(:,1)./groups(:,2);

% Create an array of the normalized duration for each segment. Thus, the
% sum of priors equals 1
priors=groups(:,2)./observations;

% Intialize two arrays with i rows and j columns where i is the number of
% unique intensity states and j is the allowed number of segments
probs=zeros(size(trace,1),number);
plist=zeros(size(trace,1),number);
convergence_criteria=.001;
converged=false;
counter=1;
while ~converged && counter<MAX_ITER
    last_priors=priors;
    last_lambda=lambda;
    
    % Iterate from 1 to the number of unique intensity states
    for i=1:size(trace,1)
        % Iterate from 1 to the allowed number of segments, calculating
        % the probabilities for each intensity, based on Poisson
        % distributions
        for j=1:number
            
            % Special case if the intensity = 0
            if trace(i,1)==0
                probs(i,j)=exp(-lambda(j)*trace(i,2));
            else
                % Poisson Distribution where trace(i,1) is the number of
                % occurences and (lambda(j)*trace(i,2) is the expected
                % value of discrete random variable X
                probs(i,j)=exp(trace(i,1)*log(lambda(j)*trace(i,2))-lambda(j)*trace(i,2)-sum(log(1:trace(i,1))));
            end
        end
        
        % Create normalization constant
        denom=dot(priors,probs(i,:));
        
        % For each segment in group i, determine the normalized
        % probability-duration value
        for j=1:number
            plist(i,j)=(priors(j)*probs(i,j))/denom;
            
            % If normalization constant is 0, only one intensity state
            % exists, i.e., the probability must be 1.
            if denom==0 
                plist(i,j)=1;
            end
        end
    end
    
    % Re-calculate priors and lambda based on plist
    for i=1:number
        priors(i)=dot(trace(:,2),plist(:,i))/observations;
        lambda(i)=dot(plist(:,i),trace(:,1))/(priors(i)*observations);
    end
    
    % If the absolute value of the difference between priors and last
    % priors is less than the convergence criteria for the whole array...
    if sum(abs(priors-last_priors)>=convergence_criteria,1)==0
        
        % Similar conditional, but for lambda
        if sum(abs(lambda-last_lambda)>=convergence_criteria,1)==0
            converged=true;
        end
    end
    counter=counter+1;
end

if ~converged
    disp("Max iter reached");
end

% Run loop one last time
for i=1:size(trace,1)
    for j=1:number
        if trace(i,1)==0
            probs(i,j)=exp(-lambda(j)*trace(i,2));
        else
            probs(i,j)=exp(trace(i,1)*log(lambda(j)*trace(i,2))-lambda(j)*trace(i,2)-sum(log(1:trace(i,1))));
        end
    end
    denom=dot(priors,probs(i,:));
    for j=1:number
        plist(i,j)=(priors(j)*probs(i,j))/denom;
        if denom==0
            plist(i,j)=1;
        end
    end
end

likelihood=0;
% Sum the likelihoods for all groups
for i=1:size(trace,1)
    likelihood=likelihood+log(dot(priors,probs(i,:)));
end
% Convert the first column to average intensity
trace(:,1)=trace(:,1)./trace(:,2);

% Make 1D array with 1 column and i rows, where i is the number of unique
% intensity states
groups=zeros(size(trace,1),1);

% For each unique intensity state (i.e., group), determine the most likely
% segment that corresponds to it
for i=1:size(groups,1)
    maxval=0;
    maxloc=1;
    
    % Iterate j from 1 to the number of segments
    for j=1:number
        
        % If the probability is bigger than the previous recorded, set the
        % new maximum value and record its location
        if plist(i,j)>maxval
            maxval=plist(i,j);
            maxloc=j;
        end
    end
    % Record the most likely segment for group i 
    groups(i)=lambda(maxloc);
end

% Initialize 1D identity array with n+1 rows, where n+1 is the number of
% segments (and n is the number of change points)
identity=zeros(size(origtrace,1),1);

% Order the groups array chronologically into the identity array.
for i=1:size(origtrace,1)
    index=1;
    % Determine what intensity from trace matches the original trace
    while trace(index,1)~=origtrace(i)
        index=index+1;
    end
    % Use the determined index to reorder the array
    identity(i)=groups(index);
end

% Recreate the set of changepoints and intensities with the newly
% determined change points
cps=[];
intens=[];
for i=2:size(identity,1)
    if identity(i)~=identity(i-1)
        cps=[cps;i-1];
        intens=[intens;identity(i-1)];
    end
end
intens=[intens;identity(end)];   
end