function [leftbound,rightbound]=CPConfidence(cp,trace,max_likelihood)
% Determines the left and right bounds of a change point. (eq 9, Haw Yang)

% Initializing variables
S=sum(trace,1); % Total sum of intensities in trace
n=size(trace,1); % Number of bins

file=fopen('ConfidenceCritValues.txt','r');
    
% If the total sum of photons is greater or equal to 3500, use the
% critical values generated for S = 3500. It is difficult to produce
% critical values beyond S = 3500 as they take weeks to generate.
if S>=3500
    fseek(file,(3500-1)*6,'bof');
    CritValue=str2double(fgetl(file));
else
    fseek(file,(S-1)*6,'bof');
    CritValue=str2double(fgetl(file));
end
fclose(file);

k=cp; % cp is the index for the change point that maximizes the
      % log-likelihood ratio
Sk=sum(trace(1:k)); % Running sum of intensity values up to the leftbound
searching=true;
leftbound=1;
rightbound=n;

% Search for the left bound. Start at k, work backwards by 1. The equation
% for the ratio is found on pg 415, "On Exact Inference for Change in a
% Poisson Sequence", https://doi.org/10.1081/STA-100002089. -S*log(S/n) is
% ignored when determining the max ratio and added back in at the end to
% increase speed.
while k>1 && searching
    Sk=Sk-trace(k);
    k=k-1;
    if Sk==0 %Implies Sk*log(Sk/k) = 0. Term was omitted for efficiency
        ratio=S*log(S/(n-k));
    else
        Snk=S-Sk;
        ratio=Sk*log(Sk/k)+Snk*log(Snk/(n-k));
    end
    
    if ratio-S*log(S/n)<max_likelihood-CritValue % (eq 9, Yang)
        searching=false;
        leftbound=k+1;
    end
    if k==1
        leftbound=1;
    end
end

% Search for the right bound. Start at k, work forward by 1. 
k=cp;
Sk=sum(trace(1:k)); % Running sum of intensity values up to the rightbound
searching=true;
while k<n && searching
    k=k+1;
    Sk=Sk+trace(k);
    if Sk==S
        ratio=S*log(S/k);
    else
        Snk=S-Sk;
        ratio=Sk*log(Sk/k)+Snk*log(Snk/(n-k));
    end
    if ratio-S*log(S/n)<max_likelihood-CritValue %(eq 9, Yang)
        searching=false;
        rightbound=k-1;
    end
    if k==n
        rightbound=n;
    end
end
end